# RStudio packages

Ce repository participatif permet d'installer en toute sécurité des packages R sur
le RStudio de l'IFB: https://rstudio.cluster.france-bioinformatique.fr/


## Principe

### Ansible
D'une manière générale, tous les installations sur le cluster de l'IFB se font
via des playbooks [Ansible](https://www.ansible.com/).
Ansible est système d'orchestration et d'automatisation de tâches d'administration.
Le principe est de ne pas intervenir sur la machine pour leurs installations et administration mais d'envoyer toutes ces tâches via des "playbooks" ou scripts Ansible.

### Git
Qui dit script dit versionning et [Git](https://en.wikipedia.org/wiki/Git) et dans notre cas GitLab. Tous les scripts d'administration sont donc "pusher" sur ce GitLab dans différents répositiory. Ainsi tout est tracé et chacun peut voir ce qu'il est en est.

### Continuous Integration (CI)
Enfin, ces playbooks Ansible ne sont pas lancés par l'un d'entre nous. Un robot d'[Intégration Continue / Continuous Integration](https://en.wikipedia.org/wiki/Continuous_integration) se charge en effet d'exécuter ces tâches dans le bonne ordre et sur les bonnes machines de manière exhautive.

### Development / Preproduction / Production
Comme tout bon projet, nous avons différent environnements de développement et de production.

L'environement de
 - développement est notre bac à sable (Sandbox)
 - preproduction se veut la plus proche de la production que possible
 - production est donc ce que les utilisateurs finaux utilisent et se doit d'être très stable et sans intéruption de service.


## Git

Parce que comme indiqué plus haut, nous avons mis des systèmes sûr d'administration (Ansible, Git, CI), nous pouvons accepter les contributions sans trop de crainte :)

### Git or GitLab

Si vous ne vous voyez pas d'utiliser `git` en ligne de commande, sachez que vous pouvez intervenir directement sur le repo Git via GitLab :
 - Création de fichier
 - Modification de fichier existant

### Branch

Git fonctionne avec des branches. La branch la plus importante est la `master`, car dans notre cas, elle correspond à la production. Ce est sur la `master` est installé automatiquement sur la production. Cette branch est protégé et n'est modifié que via Merge Request.

### Merge Request

Quand vous souhaitez apporter une modification, vous devez le faire via une nouvelle branch et dans un second temps, proposer votre modification en soumettant un Merge Request sur la branch master.

Les Merge Requests, après acceptation, vont en effet appliquer vos modifications sur la branch `master`. Seul les "Maintainers" du dépôt peuvent accepter les Merge Requests.

## Contribuer

### Ajouter un package R sur RStudio

#### 1- Modifier les bons fichiers

Il faut pour cela modifier de manière synchronisé ces 2 fichiers :
 - [preproduction/group_vars/rstudio.yml](https://gitlab.cluster.france-bioinformatique.fr/taskforce/rstudio-packages/blob/master/preproduction/group_vars/rstudio.yml)
 - [production/group_vars/rstudio.yml](https://gitlab.cluster.france-bioinformatique.fr/taskforce/rstudio-packages/blob/master/production/group_vars/rstudio.yml)

Ajouter vos packages sous l'une de ces formes, suivant leurs sources:
```
- { name: ggplot2, type: cran, state: present }
- { name: DESeq2, type: bioconductor, state: present }
- { name: bioFAM/MOFA/MOFA, type: github, state: present }
```

**NOTE :** Si vous modifier via l'interface GitLab, pensez à commiter sur la même branch.

#### 2- Soumettre un Merge Request (MR)

Via l'interface GitLab, il vous sera proposé de soumettre un Merge Request de votre branch sur la branch `master`

#### 3- Attendre le CI

Attendre que le robot d'Intégration Continue prenne en charge votre MR. Et lance le playbook sur les environements development et preproduction.

Si c'est :
 - Rouge : il y a du boulot
 - Vert : bien joué

#### 4- Attendre un Maintainers

Attendre, cette fois qu'un Maintainers review et/ou accepte votre MR

#### 5- Attendre le CI (encore)

Attendre, encore, que le robot se lance sur la production, après le merge sur la `master` pour profiter de votre package sur https://rstudio.cluster.france-bioinformatique.fr/

# Merci de vos contributions
